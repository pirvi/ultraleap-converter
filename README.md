## Description

The ```make_rpms``` script downloads the Ultraleap Gemini DEB packages and convert them to RPMS packages.

It is, currently, meant to be used on Fedora and has been tested on Fedora 39, with Gemini v5.17.1. It can probably be easily adapted to other rpm-based systems.

The DEB packages are retrieved from the https://repo.ultraleap.com repository.

Each package is extracted using dpkg-deb, some file locations are changed to conform to Fedora FHS and installation scripts are adapted.

The script makes as little modifications as possible, in the hope to be compatible with future version of the DEB packages. But it can not be future-proof...

## Instructions

- Install the dependencies

```sh
sudo dnf install sed curl dpkg rpm-build
```

- Execute the script

```sh
chmod +x make_rpms
./make_rpms
```

- Install the converted packages (to be adapted to the actual rpm files)

```sh
sudo dnf install ./ultraleap-hand-tracking-service-5.17.1.0-1.fc39.x86_64.rpm
sudo dnf install ./openxr-layer-ultraleap-1.6.5+2486adf9.CI1130164-1.fc39.x86_64.rpm
sudo dnf install ./ultraleap-hand-tracking-control-panel-3.4.1-1.fc39.x86_64.rpm
```

## Legal approval

The Ultraleap Gemini EULA states that: "You may not ... enable others to ... create an installer for the Software".
Ultraleap has been contacted through their Discord server, and the publication of this repackager has been approved by their legal team. See: https://discord.com/channels/994213697490800670/1222941107672059975/1232346103148576839